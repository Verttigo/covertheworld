module.exports = {
    apps: [
        {
            name: "CoverAll_Scrap",
            script: "./app.js",
            watch: false,
            env: {
                "Status": "prod",
                "appKey": "",
            }
        }
    ]
};
