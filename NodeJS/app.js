const cron = require("node-cron"),
    fastify = require("fastify")(),
    youtubeKey = process.env.appKey,
    fetch = require("node-fetch-commonjs"),
    fs = require("fs"),
    net = require("net"),
    {WebcastPushConnection} = require('tiktok-livestream-chat-connector');

let tiktokUsername = "finaboss88", currentViewer = 0, tiktokChatConnection = new WebcastPushConnection(tiktokUsername),
    followerCountY = 0, blockPlacedY = 0, videosDataY = new Map(),
    videosIDY = ["Qfy86Z54LHw", "LbZAMpkLosk", "QJspxGgEH0o", "_jAeePN3Rwk", "he6PLH4Y6K8"],
    followerCountT = 0, likeCount = 0, blockPlacedT = 0, videosDataT = new Map(),
    videosIDT = ["7065781602453851397", "7066057746566712581", "7066830353033415942", "7067082681065999622", "7067175677841132805"];


webserver = () => {
    fastify.get('/getBlockPlaced', (request, reply) => {
        let value = blockPlacedT + blockPlacedY;
        reply.type("text/html").send(String(value))
    })

    fastify.get('/getLiveViewers', (request, reply) => {
        reply.type("text/html").send(String(currentViewer))
    })

    fastify.listen(6666, "0.0.0.0", (err) => {
        if (err) {
            fastify.log.error(err)
            process.exit(1)
        }
        console.log("Web server has been started...")
    })
}

fetching = (url) => {
    return new Promise((data) => {
        fetch(url).then(async r => {
            data(await r.json());
        })
    })

}


getTikTokData = async () => {
    return new Promise(async (data) => {
        let videoURL = "https://m.tiktok.com/api/item/detail/?agent_user=&itemId="
        let profileURL = "https://www.tiktok.com/node/share/user/@Verttigo_?user_agent="
        let diffFollowers, diffLikes, diffComments = 0, diffViews = 0, diffShare = 0;

        if (videosIDT.length === 0) {
            //Profile Part
            let response = await fetching(profileURL);
            let simplify = response.userInfo.stats;
            diffFollowers = simplify.followerCountT - followerCountT;
            diffLikes = simplify.diffLikes - likeCount;
            followerCountT = simplify.followerCountT;
            likeCount = simplify.diffLikes;
        } else {
            let videoID = videosIDT[0];
            let response = await fetching(videoURL + videoID);
            let simplify = response.itemInfo.itemStruct.authorStats;
            diffFollowers = simplify.followerCount - followerCountT;
            diffLikes = simplify.heartCount - likeCount;
            followerCountT = simplify.followerCount;
            likeCount = simplify.heartCount;

            //videos
            simplify = response.itemInfo.itemStruct.stats;
            if (!videosDataT.get(videoID)) {
                diffComments = simplify.commentCount;
                diffViews = simplify.playCount;
                diffShare = simplify.shareCount;
                videosDataT.set(videoID, {commentCount: diffComments, viewsCount: diffViews, shareCount: diffShare})
            } else {
                let currentData = videosDataT.get(videoID);
                diffComments = simplify.commentCount - currentData.commentCount;
                diffViews = simplify.playCount - currentData.viewsCount;
                diffShare = simplify.shareCount - currentData.shareCount;
                videosDataT.set(videoID, {
                    commentCount: simplify.commentCount,
                    viewsCount: simplify.playCount,
                    shareCount: simplify.shareCount
                })
            }

            videosIDT.push(videoID);
            videosIDT.shift();

        }
        data({
            diffFollowers: Number(diffFollowers),
            diffLikes: Number(diffLikes),
            diffComments: Number(diffComments),
            diffViews: Number(diffViews),
            diffShare: Number(diffShare)
        })
    })
}

getYoutubeData = () => {
    return new Promise(async (data) => {
        let videoURL = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id={ID}&key=" + youtubeKey
        let profileURL = "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=UCgj7Y0xe3NBnGeTE-5rY78g&key=" + youtubeKey
        let diffLikes = 0, diffFollowers = 0, diffComments = 0, diffViews = 0;
        const channel = await fetching(profileURL);
        const count = channel.items[0].statistics.subscriberCount;

        diffFollowers = count - followerCountY;
        followerCountY = Number(count);

        if (videosIDY.length > 0) {
            const response = await fetching(videoURL.replace("{ID}", videosIDY[0])),
                simplify = response.items[0].statistics,
                videoID = videosIDY[0];

            if (!videosDataY.get(videoID)) {
                diffComments = simplify.commentCount;
                diffViews = simplify.viewCount;
                diffLikes = simplify.likeCount;
                videosDataY.set(videoID, {
                    commentCount: Number(diffComments),
                    viewCount: Number(diffViews),
                    likeCount: Number(diffLikes)
                })
            } else {
                let currentData = videosDataY.get(videoID);
                diffComments = simplify.commentCount - currentData.commentCount;
                diffViews = simplify.viewCount - currentData.viewCount;
                diffLikes = simplify.likeCount - currentData.likeCount;
                videosDataY.set(videoID, {
                    commentCount: Number(simplify.commentCount),
                    viewCount: Number(simplify.viewCount),
                    likeCount: Number(simplify.likeCount)
                })


                videosIDY.push(videoID);
                videosIDY.shift();
            }
            data({
                diffFollowers: Number(diffFollowers),
                diffLikes: Number(diffLikes),
                diffComments: Number(diffComments),
                diffViews: Number(diffViews)
            })
        } else {
            data({
                diffFollowers: Number(diffFollowers),
                diffLikes: Number(diffLikes),
                diffComments: Number(diffComments),
                diffViews: Number(diffViews)
            })
        }
    })

}


backup = () => {

    let mapT = []
    for (const [key, value] of videosDataT.entries()) {
        mapT.push({
            key,
            value
        })
    }

    let mapY = []
    for (const [key, value] of videosDataY.entries()) {
        mapY.push({
            key,
            value
        })
    }

    let data = {
        "tiktok": {
            followerCountT,
            likeCount,
            blockPlacedT,
            videosData: mapT
        },
        "youtube": {
            followerCountY,
            blockPlacedY,
            videosData: mapY
        }
    }

    fs.writeFile("data.json", JSON.stringify(data), () => {
    });

}

//Type 0 : Data from TikTok (data, 0, "tiktok")
//Type 1 : Data from Youtube (data, 1, "Youtube")
//Type 2 : Data from TikTok Stream Chat (data, 2, "username")
//Type 3 : Data from TikTok Stream Like(data, 3, "username")
//Type 4 : Data from TikTok Stream Share(data, 4, "username")
//Type 5 : Data from TikTok Stream Follow(data, 5, "username")

sendData = (data, type, name) => {
    let client = net.connect(1010, "127.0.0.1"), value = 0;

    client.on("error", (ex) => {
        console.log("Looks like the coverTheWorld socket is closed");
    });

    switch (type) {
        case 2:
            value = 1
            break;
        case 3:
            value = 1
            break;
        case 4:
            value = 4
            break;
        case 5:
            value = 5
            break;
        default:
            let share = data.diffShare ?? 0
            value = (data.diffFollowers * 10) + (data.diffLikes * 5) + (data.diffComments * 6) + (data.diffViews * 2) + (share * 7)
            console.log("Sent data " + value + " from " + name)
            break;
    }

    blockPlacedT += value;
    client.write(`${value}|${type}|${name}`);
    client.end();
}


recoverBackup = () => {
    if (fs.existsSync("data.json")) {
        const bck = require("./data.json")

        //Tiktok
        followerCountT = bck.tiktok.followerCountT
        likeCount = bck.tiktok.likeCount
        blockPlacedT = bck.tiktok.blockPlacedT
        for (const video of bck.tiktok.videosData) {
            videosDataT.set(video.key, video.value)
        }

        //Youtube
        followerCountY = bck.youtube.followerCountY
        blockPlacedY = bck.youtube.blockPlacedY
        for (const video of bck.youtube.videosData) {
            videosDataY.set(video.key, video.value)
        }
    }
}


connect = () => {
    // Connect to the chat (await can be used as well)
    tiktokChatConnection.connect().then(() => {
    }).catch(() => {
        setTimeout(() => {
            this.connect();
        }, 10 * 1000)
    })
}


tiktokChatConnection.on('chat', data => {
    sendData(null, 2, data.nickname)
})

tiktokChatConnection.on('social', data => {
    if (data.displayType === "pm_main_follow_message_viewer_2") {
        sendData(null, 5, data.nickname)
    } else {
        sendData(null, 4, data.nickname)
    }
})

tiktokChatConnection.on('roomUser', data => {
    currentViewer = data.viewerCount;
})

tiktokChatConnection.on('like', data => {
    sendData(null, 3, data.nickname)
})


tiktokChatConnection.on('disconnected', () => {
    connect()
})


init = () => {
    console.log("Scrapper started")

    recoverBackup();

    connect();

    cron.schedule('* * * * *', () => {
        getTikTokData().then((data) => {
            sendData(data, 0, "TikTok");
            backup();
        })
        getYoutubeData().then((data) => {
            sendData(data, 1, "Youtube");
            backup();
        })
    });
}


init();





