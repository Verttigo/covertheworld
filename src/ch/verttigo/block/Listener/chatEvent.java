package ch.verttigo.block.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class chatEvent implements Listener {

    @EventHandler
    public void onChatEvent(AsyncPlayerChatEvent event) {
        if(!event.getPlayer().isOp()) {
            event.setCancelled(true);
            return;
        }
        event.setFormat("§a[§bCoverAll§a]§c " + event.getPlayer().getDisplayName() + " §f: " + event.getMessage());
    }
}
