package ch.verttigo.block.Listener;

import ch.verttigo.block.Mechanics.cover;
import ch.verttigo.block.ScoreBoard.ScoreBoardManager;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class connectionEvent implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        ScoreBoardManager.setScoreboard(event.getPlayer());
        String message = null;

        if (event.getPlayer().isOp()) {
            message = "§a[§bCoverAll§a]§c " + event.getPlayer().getDisplayName() + " a rejoint le serveur !";
        }

        if(!event.getPlayer().hasPlayedBefore()) {
            cover.placeBlock(10);
            message = "§a[§bCoverAll§a]§c " + event.getPlayer().getDisplayName() + " a rejoint le serveur pour la première fois et vient de poser 10 TNT !";
        }

        event.getPlayer().teleport(new Location(event.getPlayer().getWorld(), 0, 120, 0));
        event.setJoinMessage(message);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        ScoreBoardManager.removeScoreboard(event.getPlayer());
        if (event.getPlayer().isOp()) {
            event.setQuitMessage("§a[§bCoverAll§a]§c " + event.getPlayer().getDisplayName() + " a quitté le serveur !");
            return;
        }
        event.setQuitMessage(null);
    }
}
