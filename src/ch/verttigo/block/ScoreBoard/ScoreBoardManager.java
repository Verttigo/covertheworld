package ch.verttigo.block.ScoreBoard;

import ch.verttigo.block.block;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class ScoreBoardManager {

    private static Map<Player, ScoreboardSign> scoreboardList;
    private static boolean link = true;

    static {
        ScoreBoardManager.scoreboardList = new HashMap<>();
    }

    public static void setScoreboard(final Player p) {
        if (!scoreboardList.containsKey(p)) {
            final ScoreboardSign scoreboard = new ScoreboardSign(p, "§a[§bCoverAll§a]");
            scoreboard.create();
            scoreboard.setLine(8, "§7§m----------------");
            scoreboard.setLine(9, "§7");
            scoreboard.setLine(10, "§bBlocks placés");
            scoreboard.setLine(11, "§a" + block.getInstance().getConfig().getInt("blockplaced"));
            scoreboard.setLine(12, "§6");
            scoreboard.setLine(13, "§7§m§m----------------");
            scoreboard.setLine(14, "§ctiktok.com/@Verttigo_");
            ScoreBoardManager.scoreboardList.put(p, scoreboard);
            return;
        }
        ScoreboardSign scoreboard = scoreboardList.get(p);
        scoreboard.setLine(11, "§a" + block.getInstance().getConfig().getInt("blockplaced"));
        if(link) {
            scoreboard.setLine(14, "§ctiktok.com/@Verttigo_");
            link = false;
        } else {
            link = true;
            scoreboard.setLine(14, "§cverttigo.fr/youtube ");
        }
    }

    public static void removeScoreboard(Player p) {
        scoreboardList.remove(p);
    }

}
