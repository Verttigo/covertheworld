package ch.verttigo.block.Socket;

import ch.verttigo.block.Mechanics.cover;
import org.bukkit.Bukkit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class server {

    private static ServerSocket server;
    private static Socket client;

    public static void start() {
        try {
            server = new ServerSocket(1010);

            while (true) {
                client = server.accept();

                String string = new BufferedReader(new InputStreamReader(client.getInputStream())).readLine();
                String[] params = string.split("\\|");

                int value = Integer.parseInt(params[0]);
                int type = Integer.parseInt(params[1]);
                String name = params[2];

                switch (type) {
                    case 2:
                        Bukkit.broadcastMessage("§a[§bCoverAll§a]§b " + name + " §cvient de poser §b" + value + " §cTNT en parlant dans le chat !");
                        break;
                    case 3:
                        Bukkit.broadcastMessage("§a[§bCoverAll§a]§b " + name + " §cvient de poser §b" + value + " §cTNT en likant le live !");
                        break;
                    case 4:
                        Bukkit.broadcastMessage("§a[§bCoverAll§a]§b " + name + " §cvient de poser §b" + value + " §cTNT en partageant le live !");
                        break;
                    case 5:
                        Bukkit.broadcastMessage("§a[§bCoverAll§a]§b " + name + " §cvient de poser §b" + value + " §cTNT en s'abonnant !");
                        break;
                    default:
                        Bukkit.broadcastMessage("§a[§bCoverAll§a]§c grâce à vos actions sur §b" + name + " §cvous venez de poser §b" + value + " §cTNT !");
                        break;
                }

                cover.placeBlock(value);

                client.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stop() {
        try {
            server.close();
            client.close();
        } catch (IOException e) {
            //Menfou des erreur je stop
        }
    }
}
