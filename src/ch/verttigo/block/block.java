package ch.verttigo.block;

import ch.verttigo.block.Listener.chatEvent;
import ch.verttigo.block.Listener.connectionEvent;
import ch.verttigo.block.Listener.weatherEvent;
import ch.verttigo.block.ScoreBoard.ScoreBoardManager;
import ch.verttigo.block.Socket.server;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class block extends JavaPlugin {


    public int loop = 0;
    public static Plugin plugin;

    @Override
    public void onEnable() {
        super.onEnable();
        plugin = this;

        //Config
        getConfig().addDefault("blockplaced", 0);
        getConfig().addDefault("index", 0);
        getConfig().options().copyDefaults(true);
        saveConfig();

        //Register World params
        World world = Bukkit.getWorld("world");
        world.setTime(6000);
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setGameRuleValue("doFireTick", "false");
        world.setSpawnLocation(0, 120, 0);

        //Register world boarder
        WorldBorder wb = world.getWorldBorder();
        wb.setCenter(0, 0);
        wb.setSize(1000);

        //Register Event
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new weatherEvent(), this);
        pm.registerEvents(new chatEvent(), this);
        pm.registerEvents(new connectionEvent(), this);

        //Start loop, only for testing
        // loop = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> cover.placeBlock(10), 0L, 20L);

        //Start scoreboard refresh
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            for (Player p : Bukkit.getOnlinePlayers()) {
                ScoreBoardManager.setScoreboard(p);
            }
        }, 0L, 40L);

        //Start socket server
        new BukkitRunnable() {
            public void run() {
                server.start();
            }
        }.runTaskAsynchronously(this);

    }

    @Override
    public void onDisable() {
        super.onDisable();
        server.stop();
    }

    public static Plugin getInstance() {
        return plugin;
    }


}
